import Rx from 'rx';
import Cycle from '@cycle/core';
import DOM from '@cycle/dom';
import {makeHistoryDriver} from '@cycle/history';
import {createHistory} from 'history';

function nextPage(i) {
  if (i > 2 || i < 1) return 1;
  else return i + 1;
}

// build list of pages
var pages =
      [1, 2, 3]
      .reduce((obj, i) => {
        obj[`/page${i}`] = {
          title: `Page ${i}`,
          main: sources => {
            return {
              DOM: Rx.Observable.of(DOM.h1(`Page ${i}`)),
              History: Rx.Observable.empty(),
              Console: Rx.Observable.empty()
            };
          }
        };
        return obj;
      }, {});

pages['/page4'] = {
  title: 'Best page',
  main: sources => {
    let click$ = sources.DOM.select('.thebest').events('click');
    return {
      DOM: Rx.Observable.of(DOM.h1('.thebest',
                                   'the very best like no one ever was')),
      History: click$.map(() => '/what'),
      Console: Rx.Observable.interval(1000).map(i => i + ' seconds elapsed')
    };
  }
};

var drivers = {
  DOM: DOM.makeDOMDriver('body'),
  History: makeHistoryDriver(createHistory()),
  Console: sink$ => {
    if (sink$ !== undefined) {
      sink$.forEach(msg => {
        if (msg !== undefined) {
          console.log(msg);
        }
        return null;
      });
    }
    return () => null;
  }
};

function main(sources) {
  // generate all pages at the start
  var newPages =
        Object.keys(pages).reduce((newPages, key) => {
          newPages[key] = pages[key].main(sources);
          return newPages;
        }, {});

  // default page
  newPages['/'] = newPages['/page1'];

  newPages[404] = {
    DOM: Rx.Observable.of(DOM.h1('404!')),
    History: Rx.Observable.empty(),
    Console: Rx.Observable.empty()
  };

  var sinks = Object.keys(drivers)
    .reduce((obj, key) => {
      obj[key] = sources.History
        .flatMapLatest(loc => {
          const page = newPages[loc.pathname];
          if (page === undefined || page === null) {
            return newPages[404][key];
          }
          else {
            return page[key];
          }
        });
      return obj;
    }, {});

  let pageNavInfo = Object.keys(pages).map(path => {
    let cls = '.' + path.substr(1);
    let click$ = sources.DOM.select(cls).events('click');
    return {
      DOM: DOM.a(cls, pages[path].title),
      History: click$.map(() => { console.log(path); return path; })
    };
  });

  sinks.DOM = sinks.DOM.map(content => {
    return DOM.div([ DOM.nav(pageNavInfo.map(obj => obj.DOM)), content ]);
  });
  sinks.History =
    Rx.Observable.merge(sinks.History,
                        Rx.Observable.merge(pageNavInfo.map(obj =>
                                                            obj.History)));

  return sinks;
}

Cycle.run(main, drivers);
